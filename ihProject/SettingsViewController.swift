//
//  SettingsViewController.swift
//  ihProject
//
//  Created by Semen Lobanov on 14.06.14.
//  Copyright (c) 2014 Semen Lobanov. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITextFieldDelegate  {

    @IBOutlet var serverAddress: UITextField!
    let serverAddresKey: String = "serverAddress"
    
    override func viewDidLoad() {
        super.viewDidLoad()
                // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(animated: Bool){
        var storedData: AnyObject? = NSUserDefaults.standardUserDefaults().objectForKey(serverAddresKey);
        if (storedData != nil){
            serverAddress.text = storedData as String;
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool{
        textField.resignFirstResponder()
        return true;
    }
    
    // Actions
    @IBAction func buttonSaveSettings_click(sender: UIButton){
        var defaults: NSUserDefaults  = NSUserDefaults.standardUserDefaults();
        if nil != defaults {
            var tmp: String = serverAddress.text;
            defaults.setObject(tmp, forKey:serverAddresKey)
        }
        NSUserDefaults.standardUserDefaults().synchronize()
        var storedData: AnyObject? = NSUserDefaults.standardUserDefaults().objectForKey(serverAddresKey);
        self.view.endEditing(true)
        
    }
    
    override func touchesBegan(touches: NSSet!, withEvent event: UIEvent!) {
        self.view.endEditing(true);
    }
}
