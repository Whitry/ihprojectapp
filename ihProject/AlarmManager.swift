//
//  AlarmManager.swift
//  ihProject
//
//  Created by Semen Lobanov on 14.06.14.
//  Copyright (c) 2014 Semen Lobanov. All rights reserved.
//
import Foundation;
import UIKit;


var alarmMgr : AlarmManager = AlarmManager()

struct alarm{
    var name = "Un-named";
    var repeat=true;
    //var tm: NSDate;
    
}

class AlarmManager: NSObject, NSURLConnectionDelegate, NSURLConnectionDataDelegate {

    var alarms = alarm[]();
    var tblAlarms: UITableView!;
    
    var data: NSMutableData = NSMutableData()
    
    func addAlarm(name: String, repeat:Bool){
        alarms.append(alarm(name: name, repeat: repeat ));
        createAlarm(alarm(name: name, repeat: repeat ))
        removeAlarm( name);
    }
    
    func connection(didReceiveResponse: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
        // Recieved a new request, clear out the data object
        self.data = NSMutableData()
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
        println(data)
        self.data.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        alarms = alarm[]();
        var err: NSError
       var jsonResult: NSArray = NSJSONSerialization.JSONObjectWithData(data, options:    NSJSONReadingOptions.MutableContainers, error: nil) as NSArray
        for i in jsonResult {
            var dict: NSDictionary  = i as NSDictionary
            alarms.append(alarm(name: dict["name"] as String, repeat: false));
        }
        tblAlarms.reloadData()
    }
    
    
    func loadAlarms(tblAlarms: UITableView){
        self.tblAlarms = tblAlarms

        var urlPath = "http://192.168.1.203:8081/api/1.0/alarms"

        var url: NSURL = NSURL(string: urlPath)
        var request: NSURLRequest = NSURLRequest(URL: url)
        
        var connection: NSURLConnection = NSURLConnection(request: request, delegate: alarmMgr, startImmediately: true)
        
        //println("Search iTunes API at URL \(url)")
        
        connection.start()
    }
    
    func createAlarm( new_alarm:alarm ){
        /*        var bodyString : CFStringRef  = ""; // Usually used for POST data
                var bodyData: CFDataRef  = CFStringCreateExternalRepresentation(kCFAllocatorDefault,
                    bodyString, CFStringBuiltInEncodings.UTF8.toRaw(), 0);
        
                var headerFieldName : CFStringRef  = "X-My-Favorite-Field";
                var headerFieldValue : CFStringRef  = "Dreams";
        
                var url: CFStringRef  = ("http://www.apple.com");
                var myURL : CFURLRef  = CFURLCreateWithString(kCFAllocatorDefault, url, nil);
        
                var requestMethod: CFStringRef  = "POST";
        var myRequest :CFHTTPMessageRef;
        withUnsafePointer(&CFHTTPMessageCreateResponse(kCFAllocatorDefault, requestMethod, myURL, kCFHTTPVersion1_1)) { unsafePointer in myRequest
            //do something with the unsafePointer
        }
                var bodyDataExt : CFDataRef  = (CFStringCreateExternalRepresentation(alloc: kCFAllocatorDefault,
                    theString: bodyData,
                    encoding:
                    CFStringBuiltInEncodings.UTF8.toRaw(),
                    lossByte: 0)) as CFDataRef;
                CFHTTPMessageSetBody(myRequest, bodyDataExt);
                CFHTTPMessageSetHeaderFieldValue(myRequest, headerFieldName, headerFieldValue);
                CFDataRef mySerializedRequest = CFHTTPMessageCopySerializedMessage(myRequest);*/
    }
    
    func removeAlarm(name: String){

        var bodyString : CFStringRef  = ""; // Usually used for POST data
        var bodyData: CFDataRef  = CFStringCreateExternalRepresentation(kCFAllocatorDefault,
            bodyString, CFStringBuiltInEncodings.UTF8.toRaw(), 0);
        
        var headerFieldName : CFStringRef  = "X-My-Favorite-Field";
        var headerFieldValue : CFStringRef  = "Dreams";

        let url: CFStringRef  = ("http://www.apple.com");
        var myURL : CFURLRef  = CFURLCreateWithString(kCFAllocatorDefault, url, nil);
        
        let requestMethod: CFStringRef  = "POST";
      //  var myRequest :CFHTTPMessageRef?
        
        var status:CFIndex = 200
        var myRequest : CFHTTPMessage? = CFHTTPMessageCreateResponse(kCFAllocatorDefault, status, requestMethod, url).takeUnretainedValue()
        
        var bodyDataExt : CFData? = CFStringCreateExternalRepresentation(kCFAllocatorDefault,
            bodyString,
            CFStringBuiltInEncodings.UTF8.toRaw(),
            0);
        CFHTTPMessageSetBody(myRequest, bodyDataExt);
        CFHTTPMessageSetHeaderFieldValue(myRequest, headerFieldName, headerFieldValue);
        var mySerializedRequest : Unmanaged<CFData> = CFHTTPMessageCopySerializedMessage(myRequest);
    }
}
