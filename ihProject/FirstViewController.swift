//
//  FirstViewController.swift
//  ihProject
//
//  Created by Semen Lobanov on 14.06.14.
//  Copyright (c) 2014 Semen Lobanov. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tblAlarms: UITableView!
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int{
        return alarmMgr.alarms.count;
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell!{
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Default")
        
//        let formatter = NSDateFormatter()
//        formatter.timeStyle = .ShortStyle
//        let tmp: String = formatter.stringFromDate(alarmMgr.alarms[indexPath.row].tm)
        cell.text = alarmMgr.alarms[indexPath.row].name
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alarmMgr.loadAlarms(tblAlarms)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        tblAlarms.reloadData()
    }
    
    func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!){
        if(editingStyle == UITableViewCellEditingStyle.Delete){
            alarmMgr.alarms.removeAtIndex(indexPath.row)
            tblAlarms.reloadData()
        }
    }

}

