//
//  SecondViewController.swift
//  ihProject
//
//  Created by Semen Lobanov on 14.06.14.
//  Copyright (c) 2014 Semen Lobanov. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var name: UITextField!
    @IBOutlet var dateTime: UIDatePicker!
    @IBOutlet var repeat: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool{
        textField.resignFirstResponder()
        return true;
    }
 
    // Actions
    @IBAction func buttonAction_click(sender: UIButton){
        alarmMgr.addAlarm(name.text, repeat: repeat.on)
        self.view.endEditing(true)
        name.text="";
        self.tabBarController.selectedIndex=0
        
    }
    
    override func touchesBegan(touches: NSSet!, withEvent event: UIEvent!) {
        self.view.endEditing(true);
    }
    
  
}

